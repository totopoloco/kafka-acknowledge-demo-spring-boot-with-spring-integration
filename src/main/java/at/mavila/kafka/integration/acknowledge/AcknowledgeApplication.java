package at.mavila.kafka.integration.acknowledge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcknowledgeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcknowledgeApplication.class, args);
	}

}
